const defaultTheme = require('tailwindcss/defaultTheme');

module.exports = {
  purge: {
    content: [
      `components/**/*.{vue,js}`,
      `slices/**/*.{vue,js}`,
      `layouts/**/*.vue`,
      `pages/**/*.vue`,
      `plugins/**/*.{js,ts}`,
      `nuxt.config.{js,ts}`
    ]
  },
  important: '#app',
  darkMode: false, // or 'media' or 'class'
  theme: {
    fontFamily: {
      sans: [...defaultTheme.fontFamily.sans],
      serif: [...defaultTheme.fontFamily.serif],
      mono: [...defaultTheme.fontFamily.mono]
    },
    custom: {
      // animation: 'cubic-bezier(0.215, 0.61, 0.355, 1)'
      animation: 'cubic-bezier(.19,1,.22,1)',
      // animation: 'cubic-bezier(.645,.045,.355,1)'
      // https://www.theglyph.studio/service-design
      transition: 'transform 1.2s cubic-bezier(0.165, 0.84, 0.44, 1) 0s, opacity 1s cubic-bezier(0.42, 0, 0.58, 1) 0s'

    },
    extend: {
      colors: {
        black: '#111111'
      },
      transitionDelay: {
        '400': '400ms',
        '600': '600ms',
        '800': '800ms',
        '900': '900ms',
        '1100': '1100ms',
        '1200': '1200ms',
        '1300': '1300ms',
        '1400': '1400ms',
      },
      typography: (theme) => ({
        DEFAULT: {
          css: {
            h1: {
              fontWeight: 600,
            }
          }
        }
      })
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/typography'),
  ]
}
