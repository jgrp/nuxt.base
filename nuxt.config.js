import Prismic from '@prismicio/client'
import smConfig from "./sm.json"
import prismicConfig from "./prismic.config"


export default async () => {
  const baseUrl = 'https://eager-murdock-cd5674.netlify.app'
  const siteName = 'Nuxt Base'

  const client = await Prismic.getApi(smConfig.apiEndpoint)
  const locales = client.languages.map(lang => lang.id)
  const defaultLocale = locales[0]

  return {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'static',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: (titleChunk) => {
      // If undefined or blank then we don't need the hyphen
      return titleChunk ? `${titleChunk} | Nuxt Base` : 'Nuxt Base';
    },
    htmlAttrs: {
      lang: defaultLocale
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { name: 'format-detection', content: 'telephone=no' },
      { name: 'robots', content: 'noindex, nofollow' }
    ],
    link: [
      // { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
      { rel: 'preconnect', href: 'https://fonts.googleapis.com' },
      { rel: 'preconnect', href: 'https://fonts.gstatic.com' } // crossOrigin
      // { rel: 'stylesheet', href: 'googlefont' },
    ]
    // script: [{
    //   src: 'https://plausible.io/js/plausible.js',
    //   'data-domain': '',
    //   defer: true,
    //   body: true
    // }]
  },
  publicRuntimeConfig: {
    sitename: siteName,
    baseUrl: baseUrl
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    '~/static/main.css',
    '~/static/locomotive-scroll/locomotive-scroll.min.css'
    // '~/node_modules/@splidejs/splide/dist/css/splide.min.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  i18n: {
    locales,
    defaultLocale,
    baseUrl
  },

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
    // add dynamic routes for sitemap
    '@/modules/sitemapRouteGenerator'
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/sitemap',
    '@nuxtjs/i18n',
    "@nuxtjs/prismic"
   ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ["@prismicio/vue"]
  },

  generate: {
    fallback: true
  },

  sitemap: {
    hostname: baseUrl,
    // excludes need to be defined in sitemapRouteGenerator also
    exclude: ['/preview', '/slice-simulator']
  },

  prismic: prismicConfig

}
}
