import prismicConfig from "./sm.json"

export default {
  endpoint: prismicConfig.apiEndpoint,
  modern: true,
  linkResolver: (doc) => {
    const prefix = doc.lang === 'en' ? '' : `/${doc.lang}`

    switch (doc.type) {
      case 'page':
        return `${prefix}/${doc.uid}`
      default:
        return prefix || '/'
    }
  },
  htmlSerializer (type, element, content, children) {
    switch (type) {
      case 'heading1':
        return /* html */ `<h1 class="mb-5 text-5xl font-light">${children.join('')}</h1>`

      case 'heading2':
        return /* html */ `<h2 class="mb-5 text-4xl font-light">${children.join('')}</h2>`

      case 'heading3':
        return /* html */ `<h3 class="mb-4 text-3xl">${children.join('')}</h3>`

      case 'heading4':
        return /* html */ `<h4 class="mb-3 text-2xl">${children.join('')}</h4>`

      case 'heading5':
        return /* html */ `<h5 class="mb-2 text-xl">${children.join('')}</h5>`

      case 'heading6':
        return /* html */ `<h6 class="mb-2 font-semibold">${children.join('')}</h6>`

      case 'paragraph':
        return /* html */ `<p class="mb-6">${children.join('')}</p>`

      case 'strong':
        return /* html */ `<strong class="font-medium">${children.join('')}</strong>`

      case 'em':
        return /* html */ `<em class="font-italic">${children.join('')}</em>`

      case 'group-o-list-item':
        return /* html */ `<ol class="mb-6 pl-4 last:mb-0 md:pl-6">${children.join('')}</ol>`

      case 'o-list-item':
        return /* html */ `<li class="mb-1 list-decimal pl-1 last:mb-0 md:pl-2">${children.join('')}</li>`

      case 'group-list-item':
        return /* html */ `<ul class="mb-6 pl-4 last:mb-0 md:pl-6">${children.join('')}</ul>`

      case 'list-item':
        return /* html */ `<li class="mb-1 list-disc pl-1 last:mb-0 md:pl-2">${children.join('')}</li>`

//       case 'preformatted':
//         return /* html */ `<pre class="mb-6 rounded bg-slate-100 p-4 text-sm last:mb-0 md:p-8 md:text-lg">
//   <code>${children.join('')}</code>
// </pre>`
      default:
        return null
    }
  }
}
