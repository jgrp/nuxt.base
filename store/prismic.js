export const state = () => ({
  alternateLanguages: [],
  navigation: {}
})

export const mutations = {
  setAlternateLanguages (state, alternateLanguages) {
    state.alternateLanguages = alternateLanguages
  },
  setNavigation (state, navigation) {
    state.navigation = navigation
  }
}

export const actions = {
  async load (store, { lang, page = { alternate_languages: [] } }) {
    const navigation = await this.$prismic.api.getSingle('navigation', { lang })
    store.commit('setNavigation', navigation)
    store.commit('setAlternateLanguages', page.alternate_languages)
  }
}
