export default {
  head() {
    if (this.page) {
      return {
        meta: [
          {
            hid: "description",
            name: "description",
            content: this.page.data.metaDescription ?? '',
          },
          {
            hid: 'og:title',
            property: 'og:title',
            content: this.page.data.title ?? ''
          },
          {
            hid: 'og:description',
            property: 'og:description',
            content: this.page.data.metaDescription ?? ''
          },
          {
            hid: 'og:type',
            property: 'og:type',
            content: 'website',
          },
          {
            hid: 'og:url',
            property: 'og:url',
            content: this.$config.baseUrl + this.page.url
          },
          {
            hid: 'og:site_name',
            property: 'og:site_name',
            content: this.$config.sitename
          }
        ],
        link: [{
          hid: 'canonical',
          name: 'canonical',
          href: this.$config.baseUrl + this.page.url
        }]
      }
    }
  },
}
